//
//  ViewController.swift
//  Practice
//
//  Created by 宋　明起 on 2018/09/03.
//  Copyright © 2018 宋　明起. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var testView: TestView!
    @IBOutlet weak var formPracticeLink: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        testView.mainLabel.text = "sup sup sup"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func onClickTablePracticeBtn(_ sender: Any) {
        let tableStoryboard = UIStoryboard(name: "table", bundle: nil)
        let vc = tableStoryboard.instantiateViewController(withIdentifier: "TableViewController") as UIViewController
        
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func onClickBtn(_ sender: Any) {
        // modal
        let tableStoryboard = UIStoryboard(name: "modal", bundle: nil)
        let vc = tableStoryboard.instantiateViewController(withIdentifier: "ModalViewController") as UIViewController
        
        present(vc, animated: true, completion: nil)
    }
    
    @IBAction func onClickSqlLiteBtn(_ sender: Any) {
        let storyboard = UIStoryboard(name: "sqlLite", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "SqlLiteViewController") as UIViewController
        
        navigationController?.pushViewController(vc, animated: true)
    }
    

}

