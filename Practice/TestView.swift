//
//  TestView.swift
//  Practice
//
//  Created by 宋　明起 on 2018/09/03.
//  Copyright © 2018 宋　明起. All rights reserved.
//

import UIKit

class TestView: UIView {
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var mainLabel: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        // load from memory
        Bundle.main.loadNibNamed("TestView", owner: self, options: nil)
        addSubview(contentView)
        
        // setting for entire view's apperance
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }

}
