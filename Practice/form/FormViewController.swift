//
//  FormViewController.swift
//  Practice
//
//  Created by 宋　明起 on 2018/09/03.
//  Copyright © 2018 宋　明起. All rights reserved.
//

import UIKit
import RxSwift

class FormViewController: UIViewController {
    
    @IBOutlet weak var nameInput: UITextField!
    @IBOutlet weak var addressInput: UITextField!
    @IBOutlet weak var toggleA: UIButton!
    
    @IBOutlet weak var saveBtn: UIButton!
    
    @IBOutlet weak var outputText: UILabel!
    @IBOutlet weak var validateBtn: UIButton!
    
    var toggleAVal: Bool = true
    
    private let viewModel = FormViewModel()
    private let bag = DisposeBag()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(FormViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        bindViewModel()
    }
    
    private func bindViewModel() {
        let inputs = FormViewModel.Input(name: nameInput.rx.text.orEmpty.asObservable(),
                                         validate: validateBtn.rx.tap.asObservable())
        
        let outputs = viewModel.transform(input: inputs)
        outputs.greeting
            .drive(outputText.rx.text)
            .disposed(by: bag)
    }
    
    @objc func dismissKeyboard() {
        print("dissmiss keyboard")
        view.endEditing(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    @IBAction func onClickToggleA(_ sender: Any) {
        toggleAVal = !toggleAVal
        
        if(toggleAVal) {
            toggleA.backgroundColor = UIColor.yellow
        }else {
            toggleA.backgroundColor = UIColor.gray
        }
    }
    
    @IBAction func onClickSave(_ sender: Any) {
        let name = nameInput.text ?? ""
        let address = addressInput.text ?? ""
        
        print("Name: " + name)
        print("Address: " + address)
        print("Toggle A val: " + String(toggleAVal))
    }
}
