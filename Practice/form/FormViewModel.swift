//
//  FormViewModel.swift
//  Practice
//
//  Created by 宋　明起 on 2018/09/06.
//  Copyright © 2018 宋　明起. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

final class FormViewModel {
    struct Input {
        let name: Observable<String>
        let validate: Observable<Void>
    }
    
    struct Output {
        let greeting: Driver<String>
    }
    
    func transform(input: Input) -> Output {
        let greeting = input.validate
            .withLatestFrom(input.name)
            .map { name in
                return "Hello \(name)!"
            }
            .startWith("")
            .asDriver(onErrorJustReturn: ":-(")
        
        return Output(greeting: greeting)
    }
}
