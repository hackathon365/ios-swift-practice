//
//  EditSqlLiteViewController.swift
//  Practice
//
//  Created by 宋　明起 on 2018/09/19.
//  Copyright © 2018 宋　明起. All rights reserved.
//

import UIKit

class EditSqlLiteViewController: UIViewController {
    @IBOutlet var nameInput: UITextField!
    @IBOutlet var scoreInput: UITextField!
    
    var scoreStore: Score? = nil
    var selectedScore: ScoreModel? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        scoreStore = Score()
        
        if let selectedScore = selectedScore {
            nameInput.text = selectedScore.name
            scoreInput.text = String(selectedScore.score)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onClickSaveBtn(sender: AnyObject) {
        let userName = nameInput.text ?? ""
        let userScore = Double(scoreInput.text ?? "0")
        let userId = selectedScore?.id
        
        scoreStore!.saveScore(userName: userName, userScore: userScore ?? 0, userId: userId)
        dismiss()
    }
    @IBAction func onCancel(_ sender: UIBarButtonItem) {
        dismiss()
    }
    
    private func dismiss() {
        dismiss(animated: true, completion: nil)
    }
    
    private func resetForm() {
        nameInput.text = ""
        scoreInput.text = ""
    }
    
}
