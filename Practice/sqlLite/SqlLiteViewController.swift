//
//  SqlLiteViewController.swift
//  Practice
//
//  Created by 宋　明起 on 2018/09/19.
//  Copyright © 2018 宋　明起. All rights reserved.
//

import UIKit

class SqlLiteViewController: TableViewController {

    private let scoreData: Score = Score()
    private var scoreList = Array<ScoreModel>()
    
    lazy var refreshControler: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        
        refreshControl.addTarget(self, action: #selector(SqlLiteViewController.handleRefresh(_:)),
                                 for: UIControlEvents.valueChanged)
        refreshControl.tintColor = UIColor.red
        
        return refreshControl
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.addSubview(refreshControler)

        // Do any additional setup after loading the view.
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        reloadData()
    }
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        reloadData()
        refreshControl.endRefreshing()
    }
    
    private func reloadData() {
        getList()
        tableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getList() {
        scoreList = scoreData.getList()
        
        for item in scoreList {
            print("ID: \(item.id), Name: \(item.name) Score: \(String(item.score)), Date: \(item.date)")
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return scoreList.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ScoreCell", for: indexPath) as? ScoreTableViewCell else {
            fatalError("The dequeued cell is not an instance of MealTableViewCell")
        }
        
        let score = scoreList[indexPath.row]
        
        cell.nameLabel.text = score.name
        cell.scoreLabel.text = String(score.score)
        cell.dateLabel.text = score.date
        
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowDetail" {
            let senderObj = sender as! UIButton
            
            let destinationNavigationController = segue.destination as! UINavigationController
            
            guard let scoreEditViewController = destinationNavigationController.topViewController as? EditSqlLiteViewController else {
                fatalError ("Unexpected destination: \(segue.destination)")
            }
            
            guard let selectedCell = senderObj.superview?.superview as? ScoreTableViewCell else {
                fatalError("Unexpected sender: \(String(describing: sender))")
            }
            
            guard let indexPath = tableView.indexPath(for: selectedCell) else {
                fatalError("The selected cell is not being displayed by the table")
            }
            
            let selectedScore = scoreList[indexPath.row]
            scoreEditViewController.selectedScore = selectedScore
        }
    }
    
    
    
    
}
