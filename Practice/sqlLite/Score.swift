//
//  Score.swift
//  Practice
//
//  Created by 宋　明起 on 2018/09/28.
//  Copyright © 2018 宋　明起. All rights reserved.
//

import Foundation
import SQLite

struct ScoreModel {
    let id: Int64
    let name: String
    let score: Double
    let date: String
}

class Score {
    var db: Connection? = nil
    
    let id = Expression<Int64>("id")
    let user = Expression<String>("name")
    let score = Expression<Double>("score")
    let date = Expression<String>("date")
    
    let table = Table("score")
    
    init() {
        var url: String? = nil
        
        if let directoryStr = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first {
            url = directoryStr.appending("/practice.sqlite3")
        }
        
        do {
            db = try Connection(url!)
            print("Success for DB connection")
            
            createScoreTable()
        } catch let error {
            print(error.localizedDescription)
        }
    }
    
    func createScoreTable() {
        do {
            try db?.run(table.create(ifNotExists: true) { t in
                t.column(id, primaryKey: true)
                t.column(user)
                t.column(score)
                t.column(date)
            })
        } catch let Result.error(message, code, statement) where code  == SQLITE_CONSTRAINT {
            print("constraint failed: \(message), in \(String(describing: statement))")
        } catch let error {
            print("Creating table failed: \(error)")
        }
        
    }
    
    func saveScore(userName: String, userScore: Double, userId: Int64?) {
        let dateObj = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let now = formatter.string(from: dateObj)
        
        do {
            if userId != nil {
                let record = table.filter(id == userId!)
                try db!.run(record.update(user <- userName, score <- userScore, date <- now))
            } else {
                try db!.run(table.insert(user <- userName, score <- userScore, date <- now))
            }
            
        } catch let Result.error(message, code, statement) where code == SQLITE_CONSTRAINT {
            print("constraint failed: \(message), in \(String(describing: statement))")
        } catch let error {
            print("Inserting record failed: \(error)")
        }
    }
    
    func getList() -> Array<ScoreModel> {
        var list = [ScoreModel]()
        do {
            list = try db!.prepare(table).map { it in ScoreModel(id: it[id], name: it[user], score: it[score], date: it[date])}
        }catch let error {
            print(error.localizedDescription)
        }
        
        return list
    }
}
