//
//  TableViewController.swift
//  Practice
//
//  Created by 宋　明起 on 2018/09/07.
//  Copyright © 2018 宋　明起. All rights reserved.
//

import UIKit
import RxSwift

class TableViewController: UITableViewController {
    private let viewModel = TableViewModel()
    private let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        bindingViewModel()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func bindingViewModel() {
        viewModel.members
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { members in
                print("got members" + String(members.count))
                self.tableView.reloadData()
            })
            .disposed(by: disposeBag)
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.members.value.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "TestTableViewCell"
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? TestTableViewCell else {
            fatalError("The dequeued cell is not an instance of TestTableViewCell")
        }
        
        let name = viewModel.members.value[indexPath.row]
        
        cell.nameLabel.text = name
        
        return cell
    }
}
