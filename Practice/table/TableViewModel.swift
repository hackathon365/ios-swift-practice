//
//  TableViewModel.swift
//  Practice
//
//  Created by 宋　明起 on 2018/09/07.
//  Copyright © 2018 宋　明起. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

final class TableViewModel {
    private let _memberList = ["Mark", "John", "Willam", "Jane", "Alex", "Andre", "Noah", "James", "Michael", "Logan", "Aidan", "Jayden", "Avery", "Carter", "Dylan", "Harper", "Luke",
    "Medison", "Owen", "Ryan", "Hunter", "Jack", "Levi", "Christan", "Julian", "Riley", "Aubery", "Jordan", "Cameron", "Angel"]
    
    var members = BehaviorRelay<[String]>(value: ["Mark"])
    
    
    init() {
        loadMemberList()
    }
    
    private func loadMemberList() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 10) {
            self.members.accept(self._memberList)
        }
    }
}
